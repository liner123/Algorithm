# 数据结构与算法

## 线性结构和非线性结构

### 1、线性结构： 

#### 顺序 || 链表

数组，链表，堆，栈等

### 2、非线性结构： 

二维数组，多维数组，广义表，树

## 稀疏数组

当一个数组中大部分元素为0，或者为同一个值得数值时，可以使用稀疏数组来保存

![](..\cal_piture\稀疏数组.png)1

### Arr ==> SpareArr

```java
/*
Arr ==> SpareArr 二维数组转稀疏数组 a(稀疏数组) b(二维数组)

  b = Arr[i][j];
  有效数字个数  sum
  a = new sparseArr int[sum+1][3];
  a[0][0] = i;
  a[0][1] = j;
  a[0][3] = sum; 
*/
    @Test
  public void ArrToSparseArr() {
        int i = 6;
        int j = 7;
        int[][] arr = new int[i][j];
        arr[4][5] = 1;
        arr[3][2] = 4;
        // 普通数组转稀疏数组
        int sum = 0;
        for (int[] ints : arr) {
            for (int anInt : ints) {
                if (anInt != 0) {
                    sum++;
                }
            }
        }
        int[][] sparseArr = new int[sum + 1][3];
        sparseArr[0][0] = i;
        sparseArr[0][1] = j;
        sparseArr[0][2] = sum;
        int count = 0;
        for (int k = 0; k < i; k++) {
            for (int l = 0; l < j; l++) {
                if (arr[k][l] != 0) {
                    count++;
                    sparseArr[count][0] = k;
                    sparseArr[count][1] = l;
                    sparseArr[count][2] = arr[k][l];
                }
            }
        }
        for (int[] ints : sparseArr) {
            for (int anInt : ints) {
                System.out.print(anInt + "\t");
            }
            System.out.println();
        }
    }
```

### SparseArr ==> Arr

```java
/*
SparseArr ==> Arr  稀疏数组转二维数组 a(稀疏数组) b(二维数组)
	a = SparseArr[i][3];
	b = new Arr[a[0][0]][a[0][1]];
	
	b[a[1][0]][a[1][1]] = a[1][2];
	....
*/
    @Test
    public void SparseArrToArr() {
        int[][] sparseArr = ArrToSparseArr();
        int[][] arr = new int[sparseArr[0][0]][sparseArr[0][1]];
        for (int i = 1; i < sparseArr.length; i++) {
            arr[sparseArr[i][0]][sparseArr[i][1]] = sparseArr[i][2];
        }
        for (int[] ints : arr) {
            for (int anInt : ints) {
                System.out.print(anInt + "\t");
            }
            System.out.println();
        }
    }
```

##  队列 Queue

### 普通队列

int front = -1   // 队头

int  rear = -1  // 队尾

入队列 rear++

出队列 front++

空 front = rear 满 rear = maxsize-1

问题 :  front ++后空间无法二次使用

### 环形队列

int front = 0;

int rear = 最后一个元素的后一个位置

满： (rear + 1) % maxsize = front 

空 : front = rear

有效数据个数 ((rear + maxsize ) - front) % maxsize

## 单向链表 LinkedList

![](..\cal_piture\链表.png)

### 链表CRUD

```java
// 头节点
    private HeroNode head = new HeroNode(1, "", "");

    class LinkedListTest {
        // 添加操作(添加到最后)
        public void add(HeroNode heroNode) {
            HeroNode temp = head;
            while (true) {
                // 最后
                if (temp.next == null) {
                    break;
                }
                temp = temp.next;
            }
            temp.next = heroNode;
        }

       // 根据 no 添加
        public void addByNo(HeroNode heroNode) {
            HeroNode temp = head;
            while (true) {
                if (temp.next.no > heroNode.no) {
                    heroNode.next = temp.next;
                    temp.next = heroNode;
                }
                temp = temp.next;
            }
        }
           // 删除
        public void delete(HeroNode heroNode) {
            HeroNode temp = head;
            while (true) {
                if (heroNode.no == temp.next.no) {
                    temp.next = temp.next.next;
                }
            }
        }

        // 修改
        public void update(HeroNode heroNode) {
            HeroNode temp = head;
            while (true) {
                if (temp.next == null) {
                    break;
                }
                if (heroNode.no == temp.no) {
                    temp.name = heroNode.name;
                    temp.nickName = heroNode.nickName;
                }
                temp = temp.next;
            }
        }

        
        public void showLinkedList() {
            if (head.next == null) {
                System.out.println("链表为空");
                return;
            }
            HeroNode temp = head.next;
            while (temp != null) {
                System.out.println(temp + "\n");
                temp = temp.next;
            }
        }
    }

    @Data
    class HeroNode {
        int no;
        String name;
        String nickName;
        HeroNode next;

        public HeroNode(int no, String name, String nickName) {
            this.no = no;
            this.name = name;
            this.nickName = nickName;
        }
    }
```

### 面试题: 获取单链表有效节点个数

```java
    private HeroNode head1 = new HeroNode(1, "", "");
    // 面试题: 获取单链表有效节点个数
    public int getLength(HeroNode heroNode) {
        if (head1.next == null) {
            return 0;
        }
        int length = 0;
        HeroNode cur = head1;
        while (cur != null) {
            length++;
            cur = cur.next;
        }
        return length;
    }
```

###     面试题: 查找单链表中的倒数第k个结点

```java
    // 面试题: 查找单链表中的倒数第k个结点
    public HeroNode findLastIndexNode(HeroNode heroNode, int index) {
        if (heroNode.next == null) {
            return null;
        }
        int length = getLength(heroNode);
        if (index <= 0 || index > length) {
            return null;
        }
        HeroNode temp = heroNode;
        for (int i = 0; i < length - index; i++) {
            temp = temp.next;
        }
        return temp;
    }
```

### 面试题: 单链表反转

```java
    // 面试题: 单链表反转
    // 思路: 遍历链表,每遍历一个就将其放在另一个新链表最前面
    public HeroNode reverseNode() {

        if (head.next == null || head.next.next == null) {
            return null;
        }
        HeroNode temp = head.next;
        HeroNode next = null; // 防止链表断裂,指向下一个结点
        HeroNode reverseHead = new HeroNode(0, "", "");
        while (temp != null) {
            // head -> temp -> temp.next
            // null -> temp.next
            next = temp.next;
            // 将temp 的下一个结点指向新节点的头节点的下一个结点，完成反向添加数据
            temp.next = reverseHead.next;
            reverseHead.next = temp;
            temp = next;
        }
        // 将旧链表的头结点指向新链表的第一个有效节点
        head.next = reverseHead.next;
        return head;
    }
```

###  面试题: 从尾到头打印单链表

```java
    // 思路1: 翻转后打印(不建议)
    // 思路2: 将链表节点放入栈，随后取出打印
    public void reverseShow(HeroNode head) {
        if (head.next == null) {
            return;
        }
        Stack<HeroNode> stack = new Stack<>();
        HeroNode temp = head.next;
        while (temp != null) {
            stack.push(temp);
            temp = temp.next;
        }
        while (stack.size() > 0) {
            System.out.println(stack.pop());
        }
    } 
```

### 面试题: 合并两个有序的单链表，合并之后的链表依然有序

```java
    // 面试题: 合并两个有序的单链表，合并之后的链表依然有序
    // 思路: 使用新的链表,给两个指针比较两个链表的 no,小的放入新的链表，指针++;
    public HeroNode mergeLinkedList(HeroNode head1, HeroNode head2) {
        HeroNode newHeroNode = new HeroNode(0, "", "");
        if (head1.next == null && head2.next == null) {
            throw new RuntimeException("两个链表都为空");
        }
        if (head1.next == null) {
            return head2;
        }
        if (head2.next == null) {
            return head1;
        }
        HeroNode temp1 = head1.next;
        HeroNode temp2 = head2.next;
        while (temp1 != null && temp2 != null) {
            // 如果 t1 小于等于 t2 就将t1加入newHeadNode,指针后移
            if (temp1.no <= temp2.no) {
                newHeroNode.next = temp1;
                temp1 = temp1.next;
                // 如果 t1 大于 t2 就将t2加入newHeadNode,指针后移
            } else {
                newHeroNode.next = temp2;
                temp2 = temp2.next;
            }
        }
        // 其中一个为null 就把另一个全部放入newHeroNode
        if (temp1 == null) {
            newHeroNode.next = temp2;
        }
        if (temp2 == null) {
            newHeroNode.next = temp1;
        }
        return newHeroNode;
    }
```

## 双向链表

pre 

next

### 双向链表CRUD

![](..\cal_piture\双向链表.png)

## 单向环形链表

### Josephu问题

Josephu问题： n个人坐成一圈，约定编号为k的人从 1 开始报数，报到m的那个人出列，然后再从下一个开始报数，m再次出列，最后都出列，出列的人的序列？

n = 5 ，k =1 , m =2

顺序为 2 -> 4 -> 1 -> 5 ->3

```java
  //  约瑟夫问题
    // 构建单向环形链表思路: first.next = first
    //                    first.next = second , second.next = first
    //                    third.next = first , second.next = third
    //                    four.next = first , third.next = four
    // 遍历: curBoy.next = first 结束
    class CircleSingleLinkedList {

        private Boy first = null;

        public void addBoy(int nums) {
            Boy temp = null;
            for (int i = 1; i < nums; i++) {
                Boy boy = new Boy(i);
                if (i == 1) {
                    first = boy;
                    first.setNext(first);
                    temp = first;
                } else {
                    temp.setNext(boy);
                    boy.setNext(first);
                    temp = boy;
                }
            }
        }

        public void showBoy() {
            if (first == null) {
                return;
            }
            Boy curBoy = first;
            while (true) {
                System.out.println("编号:" + curBoy.getNo());
                // 到最后一个了
                if (curBoy.getNext() == first) {
                    break;
                }
                curBoy = curBoy.getNext();
            }
        }
        
        // 出圈
        public void outCircle(int nums, int k, int m) {
            // 让first指针移动到 第k-1个
            for (int i = 0; i < k - 1; i++) {
                first = first.getNext();
            }
            Boy helper = first;
            // 让helper到最后一个节点
            while (true) {
                if (helper.getNext() == first) {
                    break;
                }
                helper = helper.getNext();
            }
            // 移动 m-1 次出圈
            while (helper != first) {
                for (int i = 0; i < m - 1; i++) {
                    first = first.getNext();
                    helper = helper.getNext();
                }
                // first出圈
                first = first.getNext();
                helper.setNext(first);
            }
            System.out.println("最后的小孩" + helper.getNo());
        }
    }

    @Data
    class Boy {

        private int no;
        private String name;
        private Boy next;

        public Boy(int nums) {
            this.no = nums;
        }
    }
```

## 栈 Stack

![](..\cal_piture\stack.png)

### 使用场景

![](..\cal_piture\stack_use.png)

### 实现思路及代码

top = -1

入栈： top++ , stack[top] = value

出栈： int value = stack[top]; top--  

```java
    /**
     * 数组模拟栈
     */
    class ArrayStack {
        int top = -1;
        int maxsize = 5;
        int[] stack = new int[maxsize];

        public boolean isFull() {
            return maxsize - 1 == top;
        }

        public boolean isEmpty() {
            return top == -1;
        }

        public void push(int num) {
            top++;
            stack[top] = num;
        }

        public int pop() {
            int value = stack[top];
            top--;
            return value;
        }
        public int peek(){
			int value = stack[top];
            return value;
        }
        // 运算符优先级
        public int priority(int oper) {
            if (oper == '*' || oper == '/') {
                return 1;
            } else if (oper == '+' || oper == '-') {
                return 0;
            } else {
                return -1;
            }
        }

        // 是否是操作符
        public boolean isOper(char val) {
            return val == '+' || val == '-' || val == '*' || val == '/';
        }

        // 计算
        public int cal(int num1, int num2, int oper) {
            int res = 0;
            switch (oper) {
                case '+':
                    res = num1 + num2;
                    break;
                case '-':
                    res = num1 - num2;
                    break;
                case '*':
                    res = num1 * num2;
                    break;
                case '/':
                    res = num1 / num2;
                    break;
                default:
                    break;
            }
            return res;
        }
    }
```

### 使用栈计算一个表达式的结果

```java
// 使用栈计算一个表达式的结果 7 * 2 * 2 - 5 + 1 - 5 + 3 + 4 = ?
// 思路: index 遍历表达式 , 如果是数字就入数字栈,如果是符号,
// if符号栈为空就入符号栈,
// else if 当前符号操作优先级小于或等于栈中的操作符，就需要从数栈中pop两个数，再从符号栈pop一个符号,将得到的结果入数栈,将当前符号入符号栈.
// else if 当前符号操作优先级大于栈中的操作符,直接入符号栈。
// 当表达式扫描完毕,从数栈和符号栈中pop出相应数和符号，计算。
// 最后数栈的值就是结果
// 如果是多位数，解决思路:扫描后一位,如果是数字就继续扫描字符相拼接(12)，如果是符号就将前一个数字入栈
```

```java
    @Test
    public void testStack() {
        String expression = "30+2*6-2";
        ArrayStack numberStack = new ArrayStack(10);
        ArrayStack operStack = new ArrayStack(10);
        int index = 0;
        int num1 = 0;
        int num2 = 0;
        int oper = 0;
        int res = 0;
        char ch = ' ';
        String keepNum = "";
        while (true) {
            ch = expression.substring(index, index + 1).charAt(0);
            // 是操作符
            if (operStack.isOper(ch)) {
                // 符号栈不为空
                if (!operStack.isEmpty()) {
                    int nowPriority = operStack.priority(ch);
                    int topPriority = operStack.priority(operStack.peek());
                    // 当前符号优先级小于等于栈顶符号优先级
                    if (nowPriority <= topPriority) {
                        num1 = numberStack.pop();
                        num2 = numberStack.pop();
                        oper = operStack.pop();
                        res = operStack.cal(num1, num2, oper);
                        // 计算结果入数栈
                        numberStack.push(res);
                    }
                }
                // 符号栈为空 || 优先级大于栈顶优先级 || 计算完毕后 ==> 符号入栈
                operStack.push(ch);
                // 是数字
            } else {
                // 如果是多位数，解决思路:扫描后一位,如果是数字就扫描，如果是符号就入栈
                keepNum += ch;
                // 如果最后一位就直接入栈
                if (index == expression.length() - 1) {
                    numberStack.push(Integer.parseInt(keepNum));
                    // 看下一位是不是操作符，如果是就把前一个数字入栈，如果不是就不操作继续扫描加到 keepNum
                } else if (operStack.isOper(expression.substring(index + 1, index + 2).charAt(0))) {
                    numberStack.push(Integer.parseInt(keepNum));
                    keepNum = "";
                }
            }
            index++;
            // 扫描结束
            if (index == expression.length()) {
                break;
            }
        }
        while (true) {
            // 如果符号栈为空就计算结束了
            if (operStack.isEmpty()) {
                break;
            }
            num1 = numberStack.pop();
            num2 = numberStack.pop();
            oper = operStack.pop();
            res = numberStack.cal(num2, num1, oper);
            numberStack.push(res);
        }
        System.out.println("结果是:" + numberStack.pop());
    }
```

### 前中后表达式

```java
// 前缀表达式 (3+4)x5-6  ==> -x+3456
// 中缀表达式 (3+4)x5-6 计算机不好操作
// 后缀表达式(逆波兰表达式) (3+4)x5-6 ==> 34+5x6-
```

![](..\cal_piture\逆波兰表达式.png)

### 逆波兰计算器

```java
// 逆波兰表达式: 3入栈4入栈,遇+弹出3,4相加,将7,5放入,遇到x,5x7放入栈....
@Test
public void reversePoland() {
    String expression = "3 4 + 5 x 6 -";
    Stack<String> stack = new Stack<>();
    List<String> arr = getList(expression);
    for (String s : arr) {
        if (s.matches("\\d+")) { // 是数字
            stack.push(s);
        } else {
            int num1 = Integer.parseInt(stack.pop());
            int num2 = Integer.parseInt(stack.pop());
            int res = 0;
            switch (s) {
                case "+":
                    res = num1 + num2;
                    break;
                case "-":
                    res = num2 - num1;
                    break;
                case "x":
                    res = num1 * num2;
                    break;
                case "/":
                    res = num1 / num2;
                    break;
                default:
                    break;
            }
            stack.push(res + "");
        }
    }
    System.out.println("后缀表达式计算结果：" + Integer.parseInt(stack.pop()));
}

public List<String> getList(String expression) {
    String[] spilt = expression.split(" ");
    ArrayList<String> arrayList = new ArrayList<>();
    arrayList.addAll(Arrays.asList(spilt));
    return arrayList;
}
```

### 中缀表达式转后缀表达式

#### 思路

![](..\cal_piture\中缀转后缀思路.png)

```java

    /**
     * 字符串转中缀表达式
     */
    public List<String> toInfixExpressionList(String s) {
        int i = 0;
        List<String> infixList = new ArrayList<>();
        do {
            // if 是符号直接list
            if ((s.charAt(i) < 48) || s.charAt(i) > 57) {
                infixList.add(s.charAt(i) + "");
                i++;
            } else {
                String str = "";
                while ((i < s.length()) && (s.charAt(i) >= 48) && s.charAt(i) <= 57) {
                    str += s.charAt(i);
                    i++;
                }
                infixList.add(str);
            }
        } while (i < s.length());
        return infixList;
    }
     /**
     * 中缀转后缀
     *
     */
    public List<String> toSuffixExpressionList(List<String> list) {
        Stack<String> s1 = new Stack<>();
        List<String> s2 = new ArrayList<>();
        for (String s : list) {
            // 数字直接入s2
            if (s.matches("//d+")) {
                s2.add(s);
            } else if (s.equals("(")) {
                s1.push(s);
            } else if (s.equals(")")) {
                // 如果是 ) ,则依次弹出s1栈顶的运算符,并压入s2,直到遇到左括号为止,然后将 ) 丢弃
                while (!s1.peek().equals("(")) {
                    s2.add(s1.pop());
                }
                s1.pop();
            } else {
                while (s1.size() != 0 && getValue(s1.peek()) > getValue(s)) {
                    s2.add(s1.pop());
                }
                s1.push(s);
            }
        }
        while (s1.size() != 0) {
            s2.add(s1.pop());
        }
        return s2;
    }
```

## 递归 Recursion

反复调用自身

### demo

```java
public void testRecursion(int n){
	if (n > 2){
        testRecursion(n -1);
    }    
    System.out.println("n="+n);
}
public void main(String[] args..){
    testRecursion(4);
}
//  result : n = 2 , n = 3 , n = 4
```

### 执行过程

![](..\cal_piture\recursion.png)

### **递归原则**

1）执行一个方法时，就创建一个新的受保护的独立空间(栈空间)

2）方法的局部变量是独立的，不会相互影响, 比如n变量

3）如果方法中使用的是引用类型变量(比如数组)，就会共享该引用类型的数据.

4）递归**必须向退出递归的条件逼**近，否则就是无限递归，出现StackOverflowError，死龟了:)

5）当一个方法执行完毕，或者遇到return，就会返回，**遵守谁调用，就将结果返回给谁**，同时当方法执行完毕或者返回时，该方法也就执行完毕。 

### 迷宫问题代码

```java
 // 从 1,1 到 5,6成功
/*
	1 1 1 1 1 1 1 1 
	1 0 0 0 0 0 0 1 
	1 0 0 0 0 0 0 1 
	1 1 1 0 0 0 0 1 
	1 0 0 0 0 0 0 1 
	1 0 0 0 0 0 0 1 
	1 1 1 1 1 1 1 1 
*/
public int[][] getMap() {
        int row = 7;
        int col = 8;
        int[][] map = new int[row][col];
        // 上下置为1
        for (int i = 0; i < row; i++) {
            map[i][0] = 1;
            map[i][7] = 1;
        }
        // 左右置为1
        for (int j = 0; j < col; j++) {
            map[0][j] = 1;
            map[6][j] = 1;
        }
        map[3][1] = 1;
        map[3][2] = 1;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                System.out.print(map[i][j] + " ");
            }
            System.out.println();

        }
        return map;
    }

    /**
     * @param map 地图，从1,1开始7，9结束
     * @param i   i是小球走的行
     * @param j   j 是列
     * @return 是否可以走 0 未走，1 墙，2 可以走 3 不可以走
     * 下 -> 右 -> 上 -> 左
     */
    public boolean setWay(int[][] map, int i, int j) {
        if (map[5][6] == 2) {
            return true;
        } else {
            if (map[i][j] == 0) {
                map[i][j] = 2;
                if (setWay(map, i + 1, j)) { // 下走
                    return true;
                } else if (setWay(map, i, j + 1)) { // 右走
                    return true;
                } else if (setWay(map, i - 1, j)) {
                    return true;
                } else if (setWay(map, i, j - 1)) {
                    return true;
                } else {
                    map[i][j] = 3;
                    return false;
                }
            } else { // 1,2,3
                return false;
            }
        }
    }
	/*
	1 1 1 1 1 1 1 1 
	1 2 0 0 0 0 0 1 
	1 2 2 2 0 0 0 1 
	1 1 1 2 0 0 0 1 
	1 0 0 2 0 0 0 1 
	1 0 0 2 2 2 2 1 
	1 1 1 1 1 1 1 1 
	*/
    @Test
    public void test() {
        int[][] map = getMap();
        boolean flag = setWay(map, 1, 1);
        System.out.println("flag=" + flag);
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 8; j++) {
                System.out.print(map[i][j] + " ");
            }
            System.out.println();
        }
    }
```

## 递归-回溯

### 八皇后问题

#### 思路分析

![](..\cal_piture\八皇后问题.png)

#### 代码实现

```java
/**
 * 八皇后问题
 */
int max = 8;
int[] array = new int[max];
int count = 0;

@Test
public void eightQueen() {
    check(0);
    System.out.println("解法数量:"+count);
}

public void check(int n) {
    if (n == max) { // 放好打印退出
        print();
        count++;
        return;
    }
    // 依次放入皇后，判断是否冲突 (每层 check 都有一个for,回溯)
    for (int i = 0; i < max; i++) {
        array[n] = i; // i = 0先把当前皇后放到第1列
        // 不冲突,放下一个 n+1
        if (judge(n)) {
            check(n + 1);
        }
        // 冲突 -> i++ , array[n] = 1 ,放到第2列
    }
}

//判断是否能放置
public boolean judge(int n) {
    for (int i = 0; i < n; i++) {
        // 如果两个皇后占同列 || 在斜线 |n-i| == |array[n] - array[i] 理解不了见下图
        if (array[i] == array[n] || Math.abs(n - i) == Math.abs(array[n] - array[i])) {
            return false;
        }
    }
    return true;
}
// 打印arr[] 解法
public void print() {
    for (int value: array) System.out.print(value + " ");
    System.out.println();
}
```

![](..\cal_piture\八皇后斜对角线判断算法图解.png)

## 排序算法 Sort  

### 八大排序法

​	![](..\cal_piture\排序算法介绍.png)

### 时间复杂度

#### 时间频数 T(n)

1、忽略常数项 n + 1= n

2、忽略低次项  n^2 + n = n^2

3、忽略系数 3n^2 + 7n = 5n^2

 ![](..\cal_piture\时间复杂度.png)

#### 常见排序算法时间复杂度

cc![](..\cal_piture\常见排序算法时间复杂度.png)

### 空间复杂度

占存储空间的大小

### 冒泡排序 Bubble Sorting

思路： 两两相互比较，最大的交换往后移

```java
   @Test
    public void testBubbleSort() {
        int[] a = new int[]{1, 4, 3, 2, 9, 10};
        bubbleSort(a);
        for (int i : a) {
            System.out.print(i + "\t");
        }
    }

    public void bubbleSort(int[] a) {
        boolean flag = false;
        for (int i = 0; i < a.length; i++) {
            for (int j = i + 1; j < a.length; j++) {
                if (a[i] > a[j]) {
                    flag = true;
                    swap(a, i, j);
                }
            }
            // 优化
            if(!flag){
                break;
            }else{
                flag = false;
            }
        }
    }
	//	交换
    public void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
```

### 选择排序 Select Sorting

思想： 第一次从arr[0]-arr[n-1] 中选最小的与arr[0] 交换,第二次从arr[1] - arr[n-1]，选最小的与arr[1]交换

 ```java
    public void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public void selectSort(int[] a) {

        for (int i = 0; i < a.length; i++) {
            int minIndex = i;
            int min = a[i];
            for (int j = i + 1; j < a.length; j++) {
                if (min > a[j]) {
                    minIndex = j;
                    min = a[j];
                }
            }
            swap(a, i, minIndex);
        }
    }

    @Test
    public void testSelectSort() {
        int[] a = new int[1000];
        for (int i = 0; i < 1000; i++) {
            a[i] = (int) (Math.random() * 10000);
        }
        selectSort(a);
        for (int i : a) {
            System.out.print(i + "\t");
        }
    }
 ```

### 插入排序 Insertion Sorting

思想： 一个有序表一个无序表, 无序表初始元素1个，有序表初始n-1个,将无序表元素依次拿到有序表比较排序。

```java
  @Test
    public void insertionSort() {
        int[] a = new int[1000];
        for (int i = 0; i < 1000; i++) {
            a[i] = (int) (Math.random() * 10000);
        }
        for (int i = 1; i < a.length; i++) {
            int insertIndex = i - 1;
            int insertVal = a[i];
            while (insertIndex >= 0 && insertVal < a[insertIndex]) {
                a[insertIndex + 1] = a[insertIndex]; //后移
                insertIndex--;
            }
            a[insertIndex + 1] = insertVal;
        }
        for (int i : a) {
            System.out.print(i + "\t");
        }
    }
```

### 希尔排序 Donald Shell

优化的插入排序 (避免小的数后插入后，需要后移很多次)

#### 交换法

思想: ![](..\cal_piture\shellSort.png)

```java
@Test
public void testShellSort() {
    int[] arr = {8, 9, 1, 7, 2, 3, 5, 4, 6, 0};
    shellSort(arr);
}

public void shellSort(int[] arr) {
    int temp = 0;
    for (int gap = arr.length / 2; gap > 0; gap /= 2) {
        for (int i = gap; i < arr.length; i++) {
            // 遍历各组中所有元素
            for (int j = i - gap; j >= 0; j -= gap) {
                // 如果当前元素大于加上步长的那个元素就交换
                if (arr[j] > arr[j + gap]) {
                    temp = arr[j];
                    arr[j] = arr[j + gap];
                    arr[j + gap] = temp;
                }
            }
        }
    }
}
```

#### 移动法

### 快速排序 QuickSort

对冒泡排序的改进

思想： 通过一趟排序将所有数分为 大于 某个数的和小于 某个数的两部分，左右两边再分别QuickSort。

#### 方法1

```java
@Test
public void testQuickSort() {
    int[] arr = {1, 6 , 5, 2, 4, 9, 3};
    quickSort2(arr, 0, arr.length-1);
    for (int i : arr) {
        System.out.print(i + "\t");
    }
}

public void quickSort(int[] arr, int L, int R) {
    if (L < R) {
        int M = partition(arr, L, R);
        quickSort(arr, L, M - 1);
        quickSort(arr, M + 1, R);
    }
}

public int partition(int[] arr, int L, int R) {
    int pivot = arr[R];
    int j = L;
    for (int i = L; i < R; i++) {
        if (arr[i] < pivot) {
            swap(arr, i, j);
            j++;
        }
    }
    swap(arr, j, R);
    return j;
}
```

#### 方法2

```java
public void quickSort(int[] arr, int left, int right) {
    int l = left;
    int r = right;
    // 取中间的数分段
    int pivot = arr[(left + right) / 2];
    int temp = 0;
    while (l < r) {
        // pivot左边一直找找到大于pivot的值
        while (arr[l] < pivot) {
            l++;
        }
        // pivot右边一直找找到小于pivot的值
        while (arr[r] > pivot) {
            r--;
        }
        // 左右两边已经按照左边 全部小于等于 右边
        if (l >= r) {
            break;
        }
        // 交换左边大于pivot的值和右边小于pivot的值
        temp = arr[l];
        arr[l] = arr[r];
        arr[r] = temp;
        // if交换完毕后发现arr[l] == pivot, r--; !
        if (arr[l] == pivot) {
            r--;
        }
        // if交换完毕后发现arr[r] == pivot, l++; !
        if (arr[r] == pivot) {
            l++;
        }
    }
    if (l == r) {
        l++;
        r--;
    }
    if (left < r) {
        quickSort(arr, left, r);
    }
    if (right > l) {
        quickSort(arr, l, right);
    }
}
```

归并排序


```java
/**
 * 归并排序 时间复杂度 O(nlog n )
 */
public void mergeSort(int[] arr, int L, int R) {	
		if (L != R) {
    		int M = (R + L) / 2;
    		// TODO
    		mergeSort(arr, L, M);
    		mergeSort(arr, M, R);
    		merge(arr, L, M, R);
		}
    }

public static void merge(int[] arr, int L, int M, int R) {
    int LEFT_SIZE = M - L;
    int RIGHT_SIZE = R - M + 1;

    int[] left = new int[LEFT_SIZE];
    int[] right = new int[RIGHT_SIZE];
    for (int i = 0; i < LEFT_SIZE; i++) {
        left[i] = arr[i];
    }
    for (int i = M; i < R; i++) {
        right[i - M] = arr[i];
    }
    int i = 0;
    int j = 0;
    int k = L;
    while (i < LEFT_SIZE && j < RIGHT_SIZE) {
            if (left[i] < right[j]) {
                arr[k] = left[i];
                i++;
            } else {
                arr[k] = right[j];
                j++;
            }
            k++;
        }
        while (i < LEFT_SIZE) {
            arr[k] = left[i];
            i++;
            k++;
        }
        while (j < RIGHT_SIZE) {
            arr[k] = right[j];
            j++;
            k++;
        }
    }
```

### 归并排序 MergeSort

思想： 将两有序数组依次放入一个新的数组

怎么变成两组有序数组？不断细分，直至L  = R =1

```java
/**
     * 归并排序 时间复杂度 O(nlog n )
     */
    public void mergeSort(int[] arr, int L, int R) {
        if (L != R) {
            int M = (R + L) / 2;
            // TODO
            mergeSort(arr, L, M);
            mergeSort(arr, M, R);
            merge(arr, L, M, R);
        }
    }

    public static void merge(int[] arr, int L, int M, int R) {
        int LEFT_SIZE = M - L;
        int RIGHT_SIZE = R - M + 1;

        int[] left = new int[LEFT_SIZE];
        int[] right = new int[RIGHT_SIZE];
        for (int i = 0; i < LEFT_SIZE; i++) {
            left[i] = arr[i];
        }
        for (int i = M; i < R; i++) {
            right[i - M] = arr[i];
        }
        int i = 0;
        int j = 0;
        int k = L;
        while (i < LEFT_SIZE && j < RIGHT_SIZE) {
                if (left[i] < right[j]) {
                    arr[k] = left[i];
                    i++;
                } else {
                    arr[k] = right[j];
                    j++;
                }
                k++;
            }
            while (i < LEFT_SIZE) {
                arr[k] = left[i];
                i++;
                k++;
            }
            while (j < RIGHT_SIZE) {
                arr[k] = right[j];
                j++;
                k++;
            }
        }
```

### 基数排序 RadixSort 

思想： 创建0-9十个一维数组，第一轮根据个位数放在不同桶依次取出放入原来数组，第二轮根据十位数放入（前一次放入的不清除，较短的补零），依次取出放入原来数组，第三轮根据百位数放入（放入的不清除，较短的补零），依次取出放入原来数组。

```java
@Test
public void testRadixSort() {
    int[] arr = {53, 3, 542, 748, 14, 214};
    radixSort(arr);
}

public void radixSort(int arr[]) {
    int max = arr[0];
    for (int i = 0; i < arr.length; i++) {
        if (arr[i] > max) {
            max = arr[i];
        }
    }
    int maxLength = (max + "").length();
    for (int k = 0, n = 1; k < maxLength; k++, n *= 10) {
        // 创建二维数组代替十个一位数组
        int[][] bucket = new int[10][arr.length];
        // 定义一个一维数组记录每次放入的数据个数
        int[] bucketElementCounts = new int[10];
        // 第i轮放入通
        for (int i = 0; i < arr.length; i++) {
            int digitElement = arr[i] / n % 10;
            bucket[digitElement][bucketElementCounts[digitElement]] = arr[i];
            bucketElementCounts[digitElement]++;
        }
        // 取出
        int index = 0;
        for (int i = 0; i < bucketElementCounts.length; i++) {
            if (bucketElementCounts[i] != 0) {
                // 有数据
                for (int j = 0; j < bucketElementCounts[i]; j++) {
                    // 取出每个桶元素到原数组
                    arr[index++] = bucket[i][j];
                }
            }
            //取出后将count清零
            bucketElementCounts[i] = 0;
        }
    }
    for (int i : arr) {
        System.out.print(i + "\t");
    }
}
```

### 排序算法时间复杂度比较

![]()![排序算法时间复杂度比较](..\cal_piture\排序算法时间复杂度比较.png)

## 查找算法 Search

### 顺序查找

太简单

### 二分查找

思想： mid = (left + right) / 2 ，需要查找的数与 mid 比较，小于就在左边递归查找，否则就在右边递归查找。结束递归条件，1、找到2、left > right 结束。

```java
@Test
public void testBinarySearch() {
    int[] arr = {1, 4, 6, 20, 44, 55, 66, 100};
    int aims = 20;
    int index = binarySearch(arr, 0, arr.length - 1, aims);
    System.out.println("索引：" + index);
}

public int binarySearch(int[] arr, int left, int right, int aims) {
   if (left > right) {
            return -1;
      }
    int mid = (left + right) / 2;
    if (aims < arr[mid]) {
        return binarySearch(arr, left, mid - 1, aims);
    } else if (aims > arr[mid]) {
        return binarySearch(arr, mid + 1, right, aims);
    } else {
        return mid;
    }
}
```

```java
// 找到多个
public List<Integer> binarySearch2(int[] arr, int left, int right, int aims) {
    if (left > right) {
        return new ArrayList<>();
    }
    int mid = (left + right) / 2;
    if (aims < arr[mid]) {
        return binarySearch2(arr, left, mid - 1, aims);
    } else if (aims > arr[mid]) {
        return binarySearch2(arr, mid + 1, right, aims);
    } else {
        List<Integer> list = new ArrayList<>();
        int temp = mid - 1;
        while (true) {
            if (temp < 0 || arr[temp] != aims) {
                break;
            }
            list.add(temp);
            temp--;
        }
        list.add(mid);
        temp = mid + 1;
        while (true) {
            if (temp > arr.length - 1 || arr[temp] != aims) {
                break;
            }
            list.add(temp);
            temp++;
        }
        return list;
    }
}
```

### 插值查找

和二分查找相似，就是mid选取采用自适应。

int mid = left  + (right - left) * (key - arr[left]) / (arr[right] - arr[left]);

### 斐波拉西查找