package com.athl;

import lombok.Data;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

class DemoApplicationTests {

    @Test
    public int[][] ArrToSparseArr() {
        int i = 6;
        int j = 7;
        int[][] arr = new int[i][j];
        arr[4][5] = 1;
        arr[3][2] = 4;
        // 普通数组转稀疏数组
        int sum = 0;
        for (int[] ints : arr) {
            for (int anInt : ints) {
                if (anInt != 0) {
                    sum++;
                }
            }
        }
        int[][] sparseArr = new int[sum + 1][3];
        sparseArr[0][0] = i;
        sparseArr[0][1] = j;
        sparseArr[0][2] = sum;
        int count = 0;
        for (int k = 0; k < i; k++) {
            for (int l = 0; l < j; l++) {
                if (arr[k][l] != 0) {
                    count++;
                    sparseArr[count][0] = k;
                    sparseArr[count][1] = l;
                    sparseArr[count][2] = arr[k][l];
                }
            }
        }
        for (int[] ints : sparseArr) {
            for (int anInt : ints) {
                System.out.print(anInt + "\t");
            }
            System.out.println();
        }
        return sparseArr;
    }

    @Test
    public void SparseArrToArr() {
        int[][] sparseArr = ArrToSparseArr();
        int[][] arr = new int[sparseArr[0][0]][sparseArr[0][1]];
        for (int i = 1; i < sparseArr.length; i++) {
            arr[sparseArr[i][0]][sparseArr[i][1]] = sparseArr[i][2];
        }
        for (int[] ints : arr) {
            for (int anInt : ints) {
                System.out.print(anInt + "\t");
            }
            System.out.println();
        }
    }

    // 头节点
    private HeroNode head = new HeroNode(1, "", "");

    class LinkedListTest {
        // 根据 no 添加
        public void addByNo(HeroNode heroNode) {
            HeroNode temp = head;
            while (true) {
                if (temp.next.no > heroNode.no) {
                    heroNode.next = temp.next;
                    temp.next = heroNode;
                }
                temp = temp.next;
            }
        }

        // 添加操作(添加到最后)
        public void add(HeroNode heroNode) {
            HeroNode temp = head;
            while (true) {
                // 最后
                if (temp.next == null) {
                    break;
                }
                temp = temp.next;
            }
            temp.next = heroNode;
        }

        // 删除
        public void delete(HeroNode heroNode) {
            HeroNode temp = head;
            while (true) {
                if (heroNode.no == temp.next.no) {
                    temp.next = temp.next.next;
                }
            }
        }

        // 修改
        public void update(HeroNode heroNode) {
            HeroNode temp = head;
            while (true) {
                if (temp.next == null) {
                    break;
                }
                if (heroNode.no == temp.no) {
                    temp.name = heroNode.name;
                    temp.nickName = heroNode.nickName;
                }
                temp = temp.next;
            }
        }

        public void showLinkedList() {
            if (head.next == null) {
                System.out.println("链表为空");
                return;
            }
            HeroNode temp = head.next;
            while (temp != null) {
                System.out.println(temp + "\n");
                temp = temp.next;
            }
        }
    }

    @Data
    class HeroNode {
        int no;
        String name;
        String nickName;
        HeroNode next;

        public HeroNode(int no, String name, String nickName) {
            this.no = no;
            this.name = name;
            this.nickName = nickName;
        }
    }


    private HeroNode head1 = new HeroNode(1, "", "");

    // 面试题: 获取单链表有效节点个数
    public int getLength(HeroNode heroNode) {
        if (heroNode.next == null) {
            return 0;
        }
        int length = 0;
        HeroNode cur = head1;
        while (cur != null) {
            length++;
            cur = cur.next;
        }
        return length;
    }

    // 面试题: 查找单链表中的倒数第k个结点
    public HeroNode findLastIndexNode(HeroNode heroNode, int index) {
        if (heroNode.next == null) {
            return null;
        }
        int length = getLength(heroNode);
        if (index <= 0 || index > length) {
            return null;
        }
        HeroNode temp = heroNode;
        for (int i = 0; i < length - index; i++) {
            temp = temp.next;
        }
        return temp;
    }

    // 面试题: 单链表反转
    // 思路: 遍历链表,每遍历一个就将其放在另一个新链表最前面
    public HeroNode reverseNode() {

        if (head.next == null || head.next.next == null) {
            return null;
        }
        HeroNode temp = head.next;
        HeroNode next = null; // 防止链表断裂,指向下一个结点
        HeroNode reverseHead = new HeroNode(0, "", "");
        while (temp != null) {
            // head -> temp -> temp.next
            // null -> temp.next
            next = temp.next;
            // 将temp 的下一个结点指向新节点的头节点的下一个结点，完成反向添加数据
            temp.next = reverseHead.next;
            reverseHead.next = temp;
            temp = next;
        }
        // 将旧链表的头结点指向新链表的第一个有效节点
        head.next = reverseHead.next;
        return head;
    }

    // 面试题: 从尾到头打印单链表
    // 思路1: 翻转后打印(不建议)
    // 思路2: 将链表节点放入栈，随后取出打印
    public void reverseShow(HeroNode head) {
        if (head.next == null) {
            return;
        }
        Stack<HeroNode> stack = new Stack<>();
        HeroNode temp = head.next;
        while (temp != null) {
            stack.push(temp);
            temp = temp.next;
        }
        while (stack.size() > 0) {
            System.out.println(stack.pop());
        }
    }

    // 面试题: 合并两个有序的单链表，合并之后的链表依然有序
    // 思路: 使用新的链表,给两个指针比较两个链表的 no,小的放入新的链表，指针++;
    public HeroNode mergeLinkedList(HeroNode head1, HeroNode head2) {
        HeroNode newHeroNode = new HeroNode(0, "", "");
        if (head1.next == null && head2.next == null) {
            throw new RuntimeException("两个链表都为空");
        }
        if (head1.next == null) {
            return head2;
        }
        if (head2.next == null) {
            return head1;
        }
        HeroNode temp1 = head1.next;
        HeroNode temp2 = head2.next;
        while (temp1 != null && temp2 != null) {
            // 如果 t1 小于等于 t2 就将t1加入newHeadNode,指针后移
            if (temp1.no <= temp2.no) {
                newHeroNode.next = temp1;
                temp1 = temp1.next;
                // 如果 t1 大于 t2 就将t2加入newHeadNode,指针后移
            } else {
                newHeroNode.next = temp2;
                temp2 = temp2.next;
            }
        }
        // 其中一个为null 就把另一个全部放入newHeroNode
        if (temp1 == null) {
            newHeroNode.next = temp2;
        }
        if (temp2 == null) {
            newHeroNode.next = temp1;
        }
        return newHeroNode;
    }

    //  约瑟夫问题
    // 构建单向环形链表思路: first.next = first
    //                    first.next = second , second.next = first
    //                    third.next = first , second.next = third
    //                    four.next = first , third.next = four
    // 遍历: temp.next = first 结束
    // nums = 5 ,k = 1, m = 2
    // 出圈思路: 构建一个helper指针指向最后一个节点,first指针指向第k个节点,helper和first同时后移m-1,
    // first=first.next,helper.next= first
    class CircleSingleLinkedList {

        private Boy first = null;

        public void addBoy(int nums) {
            Boy temp = null;
            for (int i = 1; i < nums; i++) {
                Boy boy = new Boy(i);
                if (i == 1) {
                    first = boy;
                    first.setNext(first);
                    temp = first;
                } else {
                    temp.setNext(boy);
                    boy.setNext(first);
                    temp = boy;
                }
            }
        }

        public void showBoy() {
            if (first == null) {
                return;
            }
            Boy curBoy = first;
            while (true) {
                System.out.println("编号:" + curBoy.getNo());
                // 到最后一个了
                if (curBoy.getNext() == first) {
                    break;
                }
                curBoy = curBoy.getNext();
            }
        }

        // 出圈
        public void outCircle(int nums, int k, int m) {
            // 让first指针移动到 第k-1个
            for (int i = 0; i < k - 1; i++) {
                first = first.getNext();
            }
            Boy helper = first;
            // 让helper到最后一个节点
            while (true) {
                if (helper.getNext() == first) {
                    break;
                }
                helper = helper.getNext();
            }
            // 移动 m-1 次出圈
            while (helper != first) {
                for (int i = 0; i < m - 1; i++) {
                    first = first.getNext();
                    helper = helper.getNext();
                }
                // first出圈
                first = first.getNext();
                helper.setNext(first);
            }
            System.out.println("最后的小孩" + helper.getNo());
        }
    }

    @Data
    class Boy {

        private int no;
        private String name;
        private Boy next;

        public Boy(int nums) {
            this.no = nums;
        }
    }

    /**
     * 数组模拟栈
     */
    class ArrayStack {
        int top = -1;
        int maxsize = 10;
        int[] stack = new int[maxsize];

        public ArrayStack(int maxsize) {
            this.maxsize = maxsize;
        }

        public boolean isFull() {
            return maxsize - 1 == top;
        }

        public boolean isEmpty() {
            return top == -1;
        }

        public void push(int num) {
            top++;
            stack[top] = num;
        }

        public int pop() {
            int value = stack[top];
            top--;
            return value;
        }

        public int peek() {
            int value = stack[top];
            return value;
        }

        // 运算符优先级
        public int priority(int oper) {
            if (oper == '*' || oper == '/') {
                return 1;
            } else if (oper == '+' || oper == '-') {
                return 0;
            } else {
                return -1;
            }
        }

        // 是否是操作符
        public boolean isOper(char val) {
            return val == '+' || val == '-' || val == '*' || val == '/';
        }

        // 计算
        public int cal(int num1, int num2, int oper) {
            int res = 0;
            switch (oper) {
                case '+':
                    res = num1 + num2;
                    break;
                case '-':
                    res = num1 - num2;
                    break;
                case '*':
                    res = num1 * num2;
                    break;
                case '/':
                    res = num1 / num2;
                    break;
                default:
                    break;
            }
            return res;
        }
    }

    // 使用栈计算一个表达式的结果 7 * 2 * 2 - 5 + 1 - 5 + 3 + 4 = ?
    // 思路: index 遍历表达式 , 如果是数字就入数字栈,如果是符号,
    // if符号栈为空就入符号栈,
    // else if 当前符号操作优先级小于或等于栈中的操作符，就需要从数栈中pop两个数，再从符号栈pop一个符号,将得到的结果入数栈,将当前符号入符号栈.
    // else if 当前符号操作优先级大于栈中的操作符,直接入符号栈。
    // 当表达式扫描完毕,从数栈和符号栈中pop出相应数和符号，计算。
    // 最后数栈的值就是结果
    @Test
    public void testStack() {
        String expression = "30+2*6-2";
        ArrayStack numberStack = new ArrayStack(10);
        ArrayStack operStack = new ArrayStack(10);
        int index = 0;
        int num1 = 0;
        int num2 = 0;
        int oper = 0;
        int res = 0;
        char ch = ' ';
        String keepNum = "";
        while (true) {
            ch = expression.substring(index, index + 1).charAt(0);
            // 是操作符
            if (operStack.isOper(ch)) {
                // 符号栈不为空
                if (!operStack.isEmpty()) {
                    int nowPriority = operStack.priority(ch);
                    int topPriority = operStack.priority(operStack.peek());
                    // 当前符号优先级小于等于栈顶符号优先级
                    if (nowPriority <= topPriority) {
                        num1 = numberStack.pop();
                        num2 = numberStack.pop();
                        oper = operStack.pop();
                        res = operStack.cal(num1, num2, oper);
                        // 计算结果入数栈
                        numberStack.push(res);
                    }
                }
                // 符号栈为空 || 优先级大于栈顶优先级 || 计算完毕后 ==> 符号入栈
                operStack.push(ch);
                // 是数字
            } else {
                // 如果是多位数，解决思路:扫描后一位,如果是数字就扫描，如果是符号就入栈
                keepNum += ch;
                // 如果是符号就入栈
                if (index == expression.length() - 1) {
                    numberStack.push(Integer.parseInt(keepNum));
                } else if (operStack.isOper(expression.substring(index + 1, index + 2).charAt(0))) {
                    numberStack.push(Integer.parseInt(keepNum));
                    keepNum = "";
                }
            }
            index++;
            // 扫描结束
            if (index == expression.length()) {
                break;
            }
        }
        while (true) {
            // 如果符号栈为空就计算结束了
            if (operStack.isEmpty()) {
                break;
            }
            num1 = numberStack.pop();
            num2 = numberStack.pop();
            oper = operStack.pop();
            res = numberStack.cal(num2, num1, oper);
            numberStack.push(res);
        }
        System.out.println("结果是:" + numberStack.pop());
    }

    // 前缀表达式 (3+4)x5-6  ==> -x+3456
    // 中缀表达式 (3+4)x5-6 计算机不好操作
    // 后缀表达式(逆波兰表达式) (3+4)x5-6 ==> 34+5x6-
    // 逆波兰表达式: 3入栈4入栈,遇+弹出3,4相加,将7,5放入,遇到x,5x7放入栈....
    @Test
    public void reversePoland() {
        String expression = "3 4 + 5 x 6 -";
        Stack<String> stack = new Stack<>();
        List<String> arr = getList(expression);
        for (String s : arr) {
            if (s.matches("\\d+")) { // 是数字
                stack.push(s);
            } else {
                int num1 = Integer.parseInt(stack.pop());
                int num2 = Integer.parseInt(stack.pop());
                int res = 0;
                switch (s) {
                    case "+":
                        res = num1 + num2;
                        break;
                    case "-":
                        res = num2 - num1;
                        break;
                    case "x":
                        res = num1 * num2;
                        break;
                    case "/":
                        res = num1 / num2;
                        break;
                    default:
                        break;
                }
                stack.push(res + "");
            }
        }
        System.out.println("后缀表达式计算结果：" + Integer.parseInt(stack.pop()));
    }

    public List<String> getList(String expression) {
        String[] spilt = expression.split(" ");
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.addAll(Arrays.asList(spilt));
        return arrayList;
    }

    /**
     * 中缀转后缀
     *
     * @param list
     * @return
     */
    public List<String> toSuffixExpressionList(List<String> list) {
        Stack<String> s1 = new Stack<>();
        List<String> s2 = new ArrayList<>();
        for (String s : list) {
            // 数字直接入s2
            if (s.matches("//d+")) {
                s2.add(s);
            } else if (s.equals("(")) {
                s1.push(s);
            } else if (s.equals(")")) {
                // 如果是 ) ,则依次弹出s1栈顶的运算符,并压入s2,直到遇到左括号为止,然后将 ) 丢弃
                while (!s1.peek().equals("(")) {
                    s2.add(s1.pop());
                }
                s1.pop();
            } else {
                while (s1.size() != 0 && getValue(s1.peek()) > getValue(s)) {
                    s2.add(s1.pop());
                }
                s1.push(s);
            }
        }
        while (s1.size() != 0) {
            s2.add(s1.pop());
        }
        return s2;
    }

    public int getValue(String s) {
        int res = 0;
        switch (s) {
            case "+":
            case "-":
                res = 0;
                break;
            case "*":
            case "/":
                res = 1;
                break;
        }
        return res;
    }

    /**
     * 字符串转中缀表达式
     */
    public List<String> toInfixExpressionList(String s) {
        int i = 0;
        List<String> infixList = new ArrayList<>();
        do {
            // if 是符号直接list
            if ((s.charAt(i) < 48) || s.charAt(i) > 57) {
                infixList.add(s.charAt(i) + "");
                i++;
            } else {
                String str = "";
                while ((i < s.length()) && (s.charAt(i) >= 48) && s.charAt(i) <= 57) {
                    str += s.charAt(i);
                    i++;
                }
                infixList.add(str);
            }
        } while (i < s.length());
        return infixList;
    }

    // 递归 Recursion
    public int[][] getMap() {
        int row = 7;
        int col = 8;
        int[][] map = new int[row][col];
        // 上下置为1
        for (int i = 0; i < row; i++) {
            map[i][0] = 1;
            map[i][7] = 1;
        }
        // 左右置为1
        for (int j = 0; j < col; j++) {
            map[0][j] = 1;
            map[6][j] = 1;
        }
        map[3][1] = 1;
        map[3][2] = 1;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                System.out.print(map[i][j] + " ");
            }
            System.out.println();

        }
        return map;
    }

    /**
     * @param map 地图，从1,1开始7，9结束
     * @param i   i是小球走的行
     * @param j   j 是列
     * @return 是否可以走 0 未走，1 墙，2 可以走 3 不可以走
     * 下 -> 右 -> 上 -> 左
     * 最短路径 ? 改变策略
     */
    public boolean setWay(int[][] map, int i, int j) {
        if (map[5][6] == 2) {
            return true;
        } else {
            if (map[i][j] == 0) {
                map[i][j] = 2;
                if (setWay(map, i + 1, j)) { // 下走
                    return true;
                } else if (setWay(map, i, j + 1)) { // 右走
                    return true;
                } else if (setWay(map, i - 1, j)) {
                    return true;
                } else if (setWay(map, i, j - 1)) {
                    return true;
                } else {
                    map[i][j] = 3;
                    return false;
                }
            } else { // 1,2,3
                return false;
            }
        }
    }

    @Test
    public void test() {
        int[][] map = getMap();
        boolean flag = setWay(map, 1, 1);
        System.out.println("flag=" + flag);
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 8; j++) {
                System.out.print(map[i][j] + " ");
            }
            System.out.println();
        }
    }

    /**
     * 八皇后问题
     */
    int max = 8;
    int[] array = new int[max];
    int count = 0;

    @Test
    public void eightQueen() {
        check(0);
        System.out.println(count);
    }

    public void check(int n) {
        if (n == max) { // 放好打印退出
            print();
            count++;
            return;
        }
        // 依次放入皇后，判断是否冲突 (每层 check 都有一个for,回溯)
        for (int i = 0; i < max; i++) {
            array[n] = i; // i = 0先把当前皇后放到第1列
            // 不冲突,放下一个 n+1
            if (judge(n)) {
                check(n + 1);
            }
            // 冲突 -> i++ , array[n] = 1 ,放到第2列
        }
    }

    //判断是否能放置
    public boolean judge(int n) {
        for (int i = 0; i < n; i++) {
            // 如果两个皇后占同列 || 在斜线 |n-i| == |array[n] - array[i] 理解不了见下图
            if (array[i] == array[n] || Math.abs(n - i) == Math.abs(array[n] - array[i])) {
                return false;
            }
        }
        return true;
    }

    public void print() {
        for (int value : array) System.out.print(value + " ");
        System.out.println();
    }

    @Test
    public void test1() {
        int[] a = new int[1000];
        for (int i = 0; i < 1000; i++) {
            a[i] = (int) (Math.random() * 10000);
        }
        bubbleSort(a);
        for (int i : a) {
            System.out.print(i + "\t");
        }
    }

    public void bubbleSort(int[] a) {
        boolean flag = false;
        for (int i = 0; i < a.length; i++) {
            for (int j = i + 1; j < a.length; j++) {
                if (a[i] > a[j]) {
                    flag = true;
                    swap(a, i, j);
                }
            }
            if (!flag) {
                break;
            } else {
                flag = false;
            }
        }
    }

    public void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public void selectSort(int[] a) {

        for (int i = 0; i < a.length; i++) {
            int minIndex = i;
            int min = a[i];
            for (int j = i + 1; j < a.length; j++) {
                if (min > a[j]) {
                    minIndex = j;
                    min = a[j];
                }
            }
            swap(a, i, minIndex);
        }
    }

    @Test
    public void testSelectSort() {
        int[] a = new int[1000];
        for (int i = 0; i < 1000; i++) {
            a[i] = (int) (Math.random() * 10000);
        }
        selectSort(a);
        for (int i : a) {
            System.out.print(i + "\t");
        }
    }

    @Test
    public void insertionSort() {
        int[] a = new int[1000];
        for (int i = 0; i < 1000; i++) {
            a[i] = (int) (Math.random() * 10000);
        }
        for (int i = 1; i < a.length; i++) {
            int insertIndex = i - 1;
            int insertVal = a[i];
            while (insertIndex >= 0 && insertVal < a[insertIndex]) {
                a[insertIndex + 1] = a[insertIndex]; //后移
                insertIndex--;
            }
            a[insertIndex + 1] = insertVal;
        }
        for (int i : a) {
            System.out.print(i + "\t");
        }
    }

    @Test
    public void testShellSort() {
        int[] arr = {8, 9, 1, 7, 2, 3, 5, 4, 6, 0};
        shellSort(arr);
    }

    public void shellSort(int[] arr) {
        int temp = 0;
        for (int gap = arr.length / 2; gap > 0; gap /= 2) {
            for (int i = gap; i < arr.length; i++) {
                // 遍历各组中所有元素
                for (int j = i - gap; j >= 0; j -= gap) {
                    // 如果当前元素大于加上步长的那个元素就交换
                    if (arr[j] > arr[j + gap]) {
                        temp = arr[j];
                        arr[j] = arr[j + gap];
                        arr[j + gap] = temp;
                    }
                }
            }
        }
    }

    public void quickSort(int[] arr, int left, int right) {
        int l = left;
        int r = right;
        // 取中间的数分段
        int pivot = arr[(left + right) / 2];
        int temp = 0;
        while (l < r) {
            // pivot左边一直找找到大于pivot的值
            while (arr[l] < pivot) {
                l++;
            }
            // pivot右边一直找找到小于pivot的值
            while (arr[r] > pivot) {
                r--;
            }
            // 左右两边已经按照左边 全部小于等于 右边
            if (l >= r) {
                break;
            }
            // 交换左边大于pivot的值和右边小于pivot的值
            temp = arr[l];
            arr[l] = arr[r];
            arr[r] = temp;
            // if交换完毕后发现arr[l] == pivot, r--; !
            if (arr[l] == pivot) {
                r--;
            }
            // if交换完毕后发现arr[r] == pivot, l++; !
            if (arr[r] == pivot) {
                l++;
            }
        }
        if (l == r) {
            l++;
            r--;
        }
        if (left < r) {
            quickSort(arr, left, r);
        }
        if (right > l) {
            quickSort(arr, l, right);
        }
    }

    @Test
    public void testQuickSort() {
        int[] arr = {1, 3, 5, 2, 4, 9, 6};
        quickSort(arr, 0, arr.length - 1);
        for (int i : arr) {
            System.out.print(i + "\t");
        }
    }

    public void quickSort2(int[] arr, int L, int R) {
        if (L < R) {
            int M = partition(arr, L, R);
            quickSort2(arr, L, M - 1);
            quickSort2(arr, M + 1, R);
        }
    }

    public int partition(int[] arr, int L, int R) {
        int pivot = arr[R];
        int j = L;
        for (int i = L; i < R; i++) {
            if (arr[i] < pivot) {
                swap(arr, i, j);
                j++;
            }
        }
        swap(arr, j, R);
        return j;
    }

    @Test
    public void testRadixSort() {
        int[] arr = {53, 3, 542, 748, 14, 214};
        radixSort(arr);
    }

    public void radixSort(int arr[]) {
        int max = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        int maxLength = (max + "").length();
        for (int k = 0, n = 1; k < maxLength; k++, n *= 10) {
            // 创建二维数组代替十个一位数组
            int[][] bucket = new int[10][arr.length];
            // 定义一个一维数组记录每次放入的数据个数
            int[] bucketElementCounts = new int[10];
            // 第i轮放入通
            for (int i = 0; i < arr.length; i++) {
                int digitElement = arr[i] / n % 10;
                bucket[digitElement][bucketElementCounts[digitElement]] = arr[i];
                bucketElementCounts[digitElement]++;
            }
            // 取出
            int index = 0;
            for (int i = 0; i < bucketElementCounts.length; i++) {
                if (bucketElementCounts[i] != 0) {
                    // 有数据
                    for (int j = 0; j < bucketElementCounts[i]; j++) {
                        // 取出每个桶元素到原数组
                        arr[index++] = bucket[i][j];
                    }
                }
                //取出后将count清零
                bucketElementCounts[i] = 0;
            }
        }
        for (int i : arr) {
            System.out.print(i + "\t");
        }
    }

    @Test
    public void testBinarySearch() {
        int[] arr = {1, 4, 6, 20, 44, 55, 66, 100};
        int aims = 20;
        int index = binarySearch(arr, 0, arr.length - 1, aims);
        System.out.println("索引：" + index);
    }

    public int binarySearch(int[] arr, int left, int right, int aims) {
        if (left > right) {
            return -1;
        }
        int mid = (left + right) / 2;
        if (aims < arr[mid]) {
            return binarySearch(arr, left, mid - 1, aims);
        } else if (aims > arr[mid]) {
            return binarySearch(arr, mid + 1, right, aims);
        } else {
            return mid;
        }
    }

    // 找到多个
    public List<Integer> binarySearch2(int[] arr, int left, int right, int aims) {
        if (left > right) {
            return new ArrayList<>();
        }
        int mid = (left + right) / 2;
        if (aims < arr[mid]) {
            return binarySearch2(arr, left, mid - 1, aims);
        } else if (aims > arr[mid]) {
            return binarySearch2(arr, mid + 1, right, aims);
        } else {
            List<Integer> list = new ArrayList<>();
            int temp = mid - 1;
            while (true) {
                if (temp < 0 || arr[temp] != aims) {
                    break;
                }
                list.add(temp);
                temp--;
            }
            list.add(mid);
            temp = mid + 1;
            while (true) {
                if (temp > arr.length - 1 || arr[temp] != aims) {
                    break;
                }
                list.add(temp);
                temp++;
            }
            return list;
        }
    }
}